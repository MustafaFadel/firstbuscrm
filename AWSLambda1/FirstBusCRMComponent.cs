﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Linq;
using Newtonsoft.Json.Linq;
using Amazon.SimpleEmail.Model;
using Amazon.SimpleEmail;
using Amazon;
using FluentFTP;

namespace AWSLambda1
{
    class FirstBusCRMComponent
    {
        bool _runSynchronously = true;
        string _log = "";
        const string SENDER = "engine@corethree.net";
        const string ftpUser = "corethree";
        const string ftpPass = "*rEqusut26Ja3r+&";
        const string ftpHost = "ftp.firstgroup.com";//"193.240.183.248"; //ftp.firstgroup.com
        string _org = "First Bus";
        int _daysInThePast = -1;
        List<StreamFile> _listFiles = new List<StreamFile>();
        DateTime _startDt = DateTime.Now;
        DateTime _endDt = DateTime.Now;
        DateTime _endDtSpecified = DateTime.MinValue;
        FtpClient _ftp = new FtpClient();
        bool _uploadToS3 = true;
        bool _uploadToFTP = true;
        bool _debug = false;
        readonly MongoClient mongoDatabaseClient;
        string[] emailRecipients;

        const bool _createDevicesFile = true;
        const bool _createUsersFile = true;
        const bool _createPurchasesFile = true;
        const bool _createTransactionsFile = true;
        const bool _createTestFile = false;
        bool _isAM = false;
        bool _isPM = false;

        public FirstBusCRMComponent(bool am, bool pm, DateTime end, int daysInPast, bool debug, string mongoConnectionString, string[] recipients)
        {
            _isAM = am;
            _isPM = pm;
            _endDtSpecified = new DateTime(end.Year, end.Month, end.Day, 23, 59, 59, 59);
            _daysInThePast = daysInPast;
            _debug = debug;
            emailRecipients = recipients;

            mongoDatabaseClient = null;
            try
            {
                string connectionString = mongoConnectionString;
                mongoDatabaseClient = new MongoClient(connectionString);
            }
            catch (Exception e)
            {
                throw e;
            }
            GenerateFiles();
        }

        #region GenerateFiles

        public string GenerateFiles()
        {
            Initialise();
            StartProcess();

            var subject = "First Bus Data Export";
            var sender = "engine@corethree.net";
            var messageBody = string.Format(@"<html>
                   <head>
                   </head>
                   <body>
                   <h1>First Bus CRM Data Export completed at {0}</h1>
                    <p>{1}</p>
                   </body>
                   </html>", DateTime.UtcNow.ToString(), _log);

            using(var client = new AmazonSimpleEmailServiceClient(RegionEndpoint.EUWest1))
            {
                var sendRequest = new SendEmailRequest
                {
                    Source = "engine@corethree.net",
                    Destination = new Destination
                    {
                        ToAddresses = emailRecipients.ToList()
                    },
                    Message = new Message
                    {
                        Subject = new Content(subject),
                        Body = new Body
                        {
                            Html = new Content
                            {
                                Charset = "UTF-8",
                                Data = messageBody
                            }
                        }
                    }
                };

                try
                {
                    Log = ("Sending email using Amazon SES...");
                    client.SendEmailAsync(sendRequest).Wait();
                    Log = ("The email was sent successfully.");
                }
                catch (Exception ex)
                {
                    Log = ("The email was not sent.");
                    Log = ("Error message: " + ex.Message);

                }
            }

            return _log;
        }
        private void Initialise()
        {
            if (_debug)
            {
                strBucket = "dataexporttest.corethree.net";
                _uploadToS3 = true;

            }
            _startDt = DateTime.Now.AddDays(-_daysInThePast);

            if (_isAM)
            {
                _startDt = new DateTime(_startDt.Year, _startDt.Month, _startDt.Day, 0, 0, 0);
                _endDt = new DateTime(_startDt.Year, _startDt.Month, _startDt.Day, 12, 0, 0, 0);
            }
            else if (_isPM)
            {
                _startDt = new DateTime(_startDt.Year, _startDt.Month, _startDt.Day, 12, 0, 0);
                _endDt = new DateTime(_startDt.Year, _startDt.Month, _startDt.Day, 23, 59, 59, 59);
            }
            else
            {
                _startDt = new DateTime(_startDt.Year, _startDt.Month, _startDt.Day, 0, 0, 0);
                _endDt = new DateTime(_startDt.Year, _startDt.Month, _startDt.Day, 23, 59, 59, 59);
            }

            if(_endDtSpecified > DateTime.MinValue.AddYears(1))
            {
                _endDt = _endDtSpecified.ToUniversalTime();
            }
        }
        private void StartProcess()
        {
            Log = ("Starting process...");

            if (!_runSynchronously)
            {
                System.Threading.Tasks.Task.Factory.StartNew(() => {
                    try
                    {
                        _listFiles.Clear();
                        Log = ("Create data from " + _startDt.ToString("u") + " to " + _endDt.ToString("u"));
                        CreateFiles();
                        // upload them to the receiving server
                        Log = ("Uploading data...");
                        UploadFiles(_listFiles);
                        Log = ("All done, good job!");
                    }
                    catch (Exception ex)
                    {
                        Log = ("Error: " + ex.Message);
                        Log = (ex.StackTrace);
                    }
                });
            }
            else
            {
                try
                {
                    _listFiles.Clear();
                    CreateFiles();
                    Log = ("Files Created.");
                    Log = (" Try Upload Files...");
                    // upload them to the receiving server
                    UploadFiles(_listFiles);
                    Log = ("All done, good job!");
                }
                catch (Exception ex)
                {
                    Log = ("Error: " + ex.Message);
                    Log = (ex.StackTrace);
                }
            }


        }

        

        private void UploadFiles(IEnumerable<StreamFile> Files)
        {
            foreach(StreamFile file in Files){
                try
                {
                    if (_uploadToS3)
                    {
                        var data = ((MemoryStream)file.Data).ToArray();
                        Log = ("Uploading '" + file.Name + "to S3...");
                        using (var ms = new MemoryStream(data))
                        {
                            UploadFileToS3(ms, file.Name);
                        }
                    }

                    if (_uploadToFTP)
                    {
                        System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                        string prepend = _debug ? "Dev/" : string.Empty;
                        _ftp.Host = ftpHost;
                        _ftp.Port = 21;
                        _ftp.Credentials = new System.Net.NetworkCredential(ftpUser, ftpPass);
                        _ftp.DataConnectionEncryption = true;
                        Log = ("Try connect to FTP...");
                        _ftp.Connect();

                        Log = ("FTP Connected.");

                        var data = ((MemoryStream)file.Data).ToArray();
                        Log = ("Transferring " + file.Name + data.Length / 1024 + "KB");
                        //file.Data.Dispose();
                        using (var ms = new MemoryStream(data))
                        {
                            using (var ftpStream = _ftp.OpenWrite(prepend + file.Name))
                            {
                                var buffer = new byte[8 * 1024];
                                int count;
                                while ((count = ms.Read(buffer, 0, buffer.Length)) > 0)
                                {
                                    ftpStream.Write(buffer, 0, count);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log = ("ERROR: " + ex.Message);
                }
            }
            //finally disconnect and dispose connection   
            _ftp.Disconnect();
            _ftp.Dispose();

        }
        private void CreateFiles()
        {
            int intLimit = Int32.MaxValue;

            IEnumerable<BsonDocument> vendors = QueryDatabaseNewConnection("Indie_Vendors", new BsonDocument()).ToList();
            List<string> listOrgs = new List<string>();

            listOrgs = vendors.Where(v => v["Name"].ToString().Contains("First Bus") /*&& v is BusCompany*/).Select(v => v["Organisation"].ToString()).Distinct().ToList();

            listOrgs.AddRange(vendors.Where(v => v["Name"].ToString().Contains("AirCoach") && !v["Name"].ToString().Equals("AirCoach") && !v["Name"].ToString().Contains("AirCoach Route 700")).Select(v => v["Organisation"].ToString()).Distinct().ToList());

            string fileName = "";
            MemoryStream fileData = null;

            Log = ("Create files...");

            //string fileNameAppend = _debug ? "_Test_" + new Random().Next().ToString() : string.Empty;

            if (_createDevicesFile)
            {
                try
                {
                    Log = ("Create Device File...");
                    if (_isAM)
                        fileName = string.Format("Devices_AM_D{0}.txt", _endDt.ToString("yyyyMMdd"));
                    else if (_isPM)
                        fileName = string.Format("Devices_PM_D{0}.txt", _endDt.ToString("yyyyMMdd"));
                    else
                        fileName = string.Format("Devices_D{0}.txt", _endDt.ToString("yyyyMMdd"));

                    //originally a part call
                    fileData = Devices(listOrgs, _startDt, _endDt);
                    _listFiles.Add(new StreamFile() { Name = fileName, ContentType = "text/plain", Data = fileData });
                    Log = ("Device File - Success");
                }
                catch (Exception ex)
                {
                    Log = ("Devices - Create Files Failed! Error: " + ex.Message);
                }
            }
            if (_createUsersFile)
            {
                try
                {
                    Log = ("Create Users File...");
                    if (_isAM)
                        fileName = string.Format("Users_Contacts_AM_D{0}.txt", _endDt.ToString("yyyyMMdd"));
                    else if (_isPM)
                        fileName = string.Format("Users_Contacts_PM_D{0}.txt", _endDt.ToString("yyyyMMdd"));
                    else
                        fileName = string.Format("Users_Contacts_D{0}.txt", _endDt.ToString("yyyyMMdd"));

                    //originally a part call
                    fileData = Users(listOrgs, _startDt, _endDt);
                    _listFiles.Add(new StreamFile() { Name = fileName, ContentType = "text/plain", Data = fileData });
                    Log = ("Users File Success");
                }
                catch (Exception ex)
                {
                    Log = ("Users - Create Files Failed! Error: " + ex.Message);
                }
            }

            if (_createPurchasesFile)
            {
                try
                {
                    Log = ("Create TicketPurchases File...");

                    if (_isAM)
                        fileName = string.Format("Ticket_Purchases_AM_D{0}.txt", _endDt.ToString("yyyyMMdd"));
                    else if (_isPM)
                        fileName = string.Format("Ticket_Purchases_PM_D{0}.txt", _endDt.ToString("yyyyMMdd"));
                    else
                        fileName = string.Format("Ticket_Purchases_D{0}.txt", _endDt.ToString("yyyyMMdd"));
                    //originally a part call
                    fileData = TicketPurchases(listOrgs, _startDt, _endDt);
                    _listFiles.Add(new StreamFile() { Name = fileName, ContentType = "text/plain", Data = fileData });
                    Log = ("TicketPurchases File Success");
                }
                catch (Exception ex)
                {
                    Log = ("Ticket Purchases - Create Files Failed! Error: " + ex.Message);
                }
            }

            if (_createTransactionsFile)
            {
                try
                {
                    Log = ("Create Purchase Transactions File...");

                    if (_isAM)
                        fileName = string.Format("Purchase_Transactions_AM_D{0}.txt", _endDt.ToString("yyyyMMdd"));
                    else if (_isPM)
                        fileName = string.Format("Purchase_Transactions_PM_D{0}.txt", _endDt.ToString("yyyyMMdd"));
                    else
                        fileName = string.Format("Purchase_Transactions_D{0}.txt", _endDt.ToString("yyyyMMdd"));

                    //originally a part call
                    fileData = PurchaseTransactions(listOrgs, _startDt, _endDt);
                    _listFiles.Add(new StreamFile() { Name = fileName, ContentType = "text/plain", Data = fileData });
                    Log = ("Purchase Transactions File Success");
                }
                catch (Exception ex)
                {
                    Log = ("Users - Create Files Failed! Error: " + ex.Message);
                }
            }
            if (_createTestFile)
            {
                try
                {
                    fileName = string.Format("fileUploadTest_D{0}.txt", _endDt.ToString("yyyyMMdd"));
                    fileData = CreateTestMemoryStream("test message");
                    _listFiles.Add(new StreamFile() { Name = fileName, ContentType = "text/plain", Data = fileData });
                    Log = ("Test file Created");
                }
                catch (Exception ex)
                {
                    Log = ("Test Files Failed Error: " + ex.Message);
                }
            }
        }
        private class StreamFile
        {
            public string Name;
            public string ContentType;
            public Stream Data;
            public string TempFileName;
        }
        string strAWSAccessKeyID = "AKIAJMCFV2W7T5BQQ6FQ";
        string strAWSAccessKey = "gRcLVud9nHLszFbI0dOwCdp1FyVrGQL6noBpPd4s";
        string strBucket = "firstcrmexport.corethree.net";

        private void UploadFileToS3(Stream Data, string FileName)
        {
            var s3Client = new Amazon.S3.AmazonS3Client(strAWSAccessKeyID, strAWSAccessKey, new Amazon.S3.AmazonS3Config()
            {
                RegionEndpoint = Amazon.RegionEndpoint.EUWest1
            });
            var s3Transfer = new Amazon.S3.Transfer.TransferUtility(s3Client);
            s3Transfer.Upload(Data, strBucket, FileName);
            Log = (FileName + " transferred to S3.");
        }
        private void _ftp_ValidateCertificate(System.Net.FtpClient.FtpClient control, System.Net.FtpClient.FtpSslValidationEventArgs e)
        {
            e.Accept = true;
        }
        private MemoryStream CreateTestMemoryStream(string message)
        {
            var s = new MemoryStream();
            long _streamLength = 0;

            using (var sw = new StreamWriter(s))
            {
                sw.WriteLine(message);
                sw.Flush();
                _streamLength = s.Length;
            }
            return s;
        }

        #endregion

        #region Devices

        IEnumerable<string> _orgs = null;

        public MemoryStream Devices(IEnumerable<string> orgs, DateTime startDt, DateTime endDt)
        {
            InitialiseDevicesCRM(orgs, startDt, endDt);
            GenerateDataDevicesCRM();
            return GenerateMemoryStreamDevicesCRM("Processing First Bus Devices");
        }
        private void GenerateDataDevicesCRM()
        {
            GetDeviceData();
        }
        private MemoryStream GenerateMemoryStreamDevicesCRM(string name)
        {
            ReportStructDevicesCRM rs = new ReportStructDevicesCRM();
            var dictMap = MapDataDevicesCRM();
            var s = new MemoryStream();
            using (var sw = new StreamWriter(s))
            {
                sw.WriteLine("DeviceID,Device,OS,AppVersion,User ID,DateTime");

                foreach (BsonDocument u in users){
                    
                    rs.DeviceId = u["tags"]["LastDeviceID"].ToString();
                    var lastUserAgent = u["tags"]["LastUserAgent"].ToString();
                    rs.Device = GetUserAgentParts(lastUserAgent, "device");
                    rs.Os = GetUserAgentParts(lastUserAgent, "os");
                    rs.AppVersion = GetUserAgentParts(lastUserAgent, "appversion");
                    rs.UserId = u["_id"].ToString();
                    rs.Datetime = ConvertDate(DateTime.Parse(u["Created"].ToString()));
                    sw.WriteLine(string.Join(",",
                                             rs.DeviceId,
                                             rs.Device.Replace(",", string.Empty),
                                             rs.Os.Replace(",", string.Empty),
                                             rs.AppVersion.Replace(",", string.Empty),
                                             rs.UserId,
                                             rs.Datetime));
                }

                sw.Flush();
                _streamLength = s.Length;
            }
            return s;
        }
        private void InitialiseDevicesCRM(IEnumerable<string> orgs, DateTime startDt, DateTime endDt)
        {
            _orgs = orgs;
            _startDt = startDt;
            _endDt = endDt;
        }
        private void GetDeviceData()
        {
            BsonDocument query = GetQueryDevicesCRM(_orgs, _startDt.ToString(), _endDt.ToString());
            users = QueryDatabaseExistingConnection(mongoDatabaseClient, "Users", query).ToList();
            users = users.Where(u => u.ToBsonDocument().Contains("tags") && u["tags"].ToBsonDocument().Contains("LastDeviceID")
                                  && u["tags"].ToBsonDocument().Contains("LastUserAgent") && !u["Email"].ToString().Contains("@mailinator.com")).ToList();
        }
        private BsonDocument GetQueryDevicesCRM(IEnumerable<string> orgs, string startDate = "", string endDate = "")
        {
            BsonDocument query = new BsonDocument();
            query.Add("AssociatedOrganisations", new BsonDocument("$in", includeOrgs()));

            // date filtering
            if (!string.IsNullOrWhiteSpace(startDate) && !string.IsNullOrWhiteSpace(endDate))
            {
                var start = DateTime.Parse(startDate);
                var end = DateTime.Parse(endDate);

                BsonArray arr = new BsonArray();
                arr.Add(new BsonDocument().Add("Created", new BsonDocument().Add("$gte", start).Add("$lte", end)));
                arr.Add(new BsonDocument().Add("tags.LastDeviceUpdated", new BsonDocument().Add("$gte", start.ToString("yyyy-MM-dd HH:mm:ssZ")).Add("$lte", end.ToString("yyyy-MM-dd HH:mm:ssZ"))));
                query.Add("$or", arr);
            }
            return query;
        }
        private BsonArray includeOrgs()
        {
            return new BsonArray {
                    "First Bus",
                    "First Bus Aberdeen",
                    "First Bus Airport Flyer",
                    "First Bus Bath",
                    "First Bus Basildon",
                    "First Bus Bath and Bristol",
                    "First Bus Berkshire",
                    "First Bus Boardmasters Festival", //FT-274
                    "First Bus Bradford",
                    "First Bus Brentwood",
                    "First Bus Bright Bus Tours - Edinburgh", //FT-347
                    "First Bus Bristol",
                    "First Bus Broomfield Hospital",
                    "First Bus Buses of Somerset",
                    "First Bus Calderdale & Huddersfield",
                    "First Bus Chelmsford",
                    "First Bus Chelmsford City Council",
                    "First Bus Clacton and Walton",
                    "First Bus Colchester",
                    "First Bus Cymru",
                    "First Bus Devon",
                    "First Bus Doncaster",
                    "First Bus Dorset",
                    "First Bus East Lothian",
                    "First Bus Eastern Counties Communities",
                    "First Bus Ed Sheeran (Ipswich)", //FT-274
                    "First Bus Essex County Council",
                    "First Bus Essex More",
                    "First Bus Essex Network",
                    "First Bus Essex Park & Ride",
                    "First Bus Essex Students",
                    "First Bus Glasgow",
                    "First Bus Glasgow Airport Express",
                    "First Bus Greater Glasgow",
                    "First Bus Glasgow Shuttle",
                    "First Bus Greater Manchester",
                    "First Bus Halifax Calder Valley & Huddersfield",
                    "First Bus Hampshire",
                    "First Bus Heathrow RailAir",
                    "First Bus Ipswich",
                    "First Bus Ipswich Park & Ride",
                    "First Bus Ipswich - Park & Ride",
                    "First Bus Kernow",
                    "First Bus Leeds",
                    "First Bus Leeds Festival", //FT-274
                    "First Bus Leicester",
                    "First Bus Metrobus",
                    "First Bus Norfolk & Suffolk",
                    "First Bus Norfolk & Waveney",
                    "First Bus North & Mid Essex",
                    "First Bus Portsmouth Fareham and Gosport",
                    "First Bus RailAir",
                    "First Bus Rotherham",
                    "First Bus Sheffield",
                    "First Bus Sheffield Sunday League",
                    "First Bus South Essex",
                    "First Bus Southend Metro",
                    "First Bus Southampton",
                    "First Bus Stirling and Falkirk",
                    "First Bus The Potteries",
                    "First Bus Thurrock",
                    "First Bus University of East Anglia",
                    "First Bus University of Essex",
                    "First Bus West Lothian",
                    "First Bus West of England",
                    "First Bus West of England Area",
                    "First Bus Weston super Mare",
                    "First Bus Worcester",
                    "First Bus Worcestershire",
                    "First Bus Yeovil",
                    "First Bus York",
                    "First Bus Penryn Campus",
                    "First Bus X10 and X30",
                    "First Bus RailAir Guildford",
                    "AirCoach 700 Airport – Leopardstown",
                    "AirCoach 700 Leopardstown – Airport",
                    "AirCoach 702 Airport – Greystones",
                    "AirCoach 702 Greystones – Airport",
                    "AirCoach 703 Airport – Killiney",
                    "AirCoach 703 Killiney – Airport",
                    "AirCoach 704X Cork – Dublin",
                    "AirCoach 704X Dublin – Cork",
                    "AirCoach 705X Belfast – Dublin",
                    "AirCoach 705X Dublin – Belfast"
                };
        }
        
        private Dictionary<string, MappingRule<ReportStructDevicesCRM>> MapDataDevicesCRM()
        {
            var dictMap = new Dictionary<string, MappingRule<ReportStructDevicesCRM>>() {
                { "DeviceId", r => r.DeviceId },
                { "Device", r => r.Device },
                { "Os", r => r.Os },
                { "AppVersion", r => r.AppVersion},
                { "UserID", r => r.UserId },
                { "Datetime", r => r.Datetime.ToString() }
            };
            return dictMap;
        }
        public struct ReportStructDevicesCRM
        {
            public string DeviceId;
            public string Device;
            public string Os;
            public string AppVersion;
            public string UserId;
            public string Datetime;
        }
        private string ConvertDate(DateTime dateToConvert)
        {
            return dateToConvert.ToString("MM/dd/yyyyTHH:mm:ss.fffG\\MTzzz");
        }

        #endregion

        #region Users

        const string cstrActivityTypeID_OptOut = "kQomDECzrzC";
        const string cstrActivityTypeID_OptIn = "SVuvvAjtjuG";
        List<string> _users;
        IEnumerable<string> _createdUsers;
        IEnumerable<string> _marketingUsers;

        public MemoryStream Users(IEnumerable<string> orgs, DateTime startDt, DateTime endDt)
        {
            InitialiseUsersCRM(orgs, startDt, endDt);
            GenerateDataUsersCRM();
            return GenerateMemoryStreamUsersCRM("Processing First Bus Users");
        }
        private void GenerateDataUsersCRM()
        {
            GetUserDataUsersCRM();
        }
        private MemoryStream GenerateMemoryStreamUsersCRM(string name)
        {
            ReportStructUsersCRM rs = new ReportStructUsersCRM();
            var dictMap = MapDataUsersCRM();
            var s = new MemoryStream();
            using (var sw = new StreamWriter(s))
            {
                //GOBRIEN GDPR Version
                sw.WriteLine(@"Account ID,User ID,First Name,Last Name,Email Address,Address,Town,County,Country,Postcode,PhoneNumber,Created,Updated,Marketing Opt-In Date,New Preference,Yes_Please, No_thanks,age16,Organisation1,Organisation2,Organisation3,Organisation4,Organisation5,First Purchase Date,First Purchase Type,Last Purchase Date,Last Purchase Type,Last Activation,Purchase Count,Source,Last Device ID");


                _users.ForEach(userId =>
                {
                    //GOBRIEN - GDPR changes  
                    var u = QueryDatabaseExistingConnection(mongoDatabaseClient, "Users", new BsonDocument().Add("_id", userId)).FirstOrDefault();

                    //Correct account ID for nightly runs
                    rs.AccountId = "0010Y000013l25DQAQ";
                    rs.UserId = u["_id"].ToString();
                    var splitName = u["Name"].ToString().Split(new[] { ' ' }, 2);
                    rs.FirstName = !string.IsNullOrEmpty(splitName.ToString()) ? splitName[0] : string.Empty;
                    if (splitName.Count() > 1)
                        rs.LastName = splitName[1].ToString();
                    else
                        rs.LastName = string.Empty;

                    rs.Email = u.Contains("Email") && !string.IsNullOrEmpty(u["Email"].ToString()) ? u["Email"].ToString() : string.Empty;
                    rs.Address = u.Contains("tags") && u["tags"].ToBsonDocument().Contains("Details_Address") ? u["tags"]["Details_Address"].ToString() : string.Empty;
                    rs.Town = u.Contains("tags") && u["tags"].ToBsonDocument().Contains("Details_Town") ? u["tags"]["Details_Town"].ToString() : string.Empty; 
                    rs.County = u.Contains("tags") && u["tags"].ToBsonDocument().Contains("Details_County") ? u["tags"]["Details_County"].ToString() : string.Empty; 
                    rs.Country = u.Contains("tags") && u["tags"].ToBsonDocument().Contains("Details_Country") ? u["tags"]["Details_Country"].ToString() : string.Empty; 
                    rs.Postcode = u.Contains("tags") && u["tags"].ToBsonDocument().Contains("Details_Postcode") ? u["tags"]["Details_Postcode"].ToString() : string.Empty; 
                    rs.PhoneNumber = u.Contains("PhoneNumber") && !string.IsNullOrEmpty(u["PhoneNumber"].ToString()) ? u["PhoneNumber"].ToString() : string.Empty;
                    rs.Created = ConvertDate(DateTime.Parse(u["Created"].ToString()));
                    rs.Updated = ConvertDate(_startDt);

                    BsonDocument queryDoc = new BsonDocument("ActivityTypeID", cstrActivityTypeID_Tickets);
                    queryDoc.Add("User.ID", u["_id"].ToString());
                    queryDoc.Add("Organisation", new BsonDocument("$in", includeOrgs()));

                    var ticketsBought = QueryDatabaseExistingConnection(mongoDatabaseClient, "Log_Activity", queryDoc).OrderBy(t => DateTime.Parse(t["DT"].ToString()));
                    if (ticketsBought != null && ticketsBought.Count() > 0)
                    {
                        rs.FirstPurchaseDate = ConvertDate(DateTime.Parse(ticketsBought.First()["DT"].ToString()));
                        rs.FirstPurchaseType = ticketsBought.First()["Name"].ToString().Replace("–", "-");
                        rs.LastPurchaseDate = ConvertDate(DateTime.Parse(ticketsBought.Last()["DT"].ToString()));
                        rs.LastPurchaseType = ticketsBought.Last()["Name"].ToString().Replace("–", "-");
                        var lastTicket = ticketsBought.Last();
                        rs.LastActivation = lastTicket["MetaData"].ToBsonDocument().Contains("Ticket_Activated") ? ConvertDate(DateTime.Parse(lastTicket["MetaData"]["Ticket_Activated"].ToString())) : string.Empty;
                        rs.PurchaseCount = ticketsBought.Count();
                    }
                    else
                    {
                        rs.FirstPurchaseDate = string.Empty;
                        rs.FirstPurchaseType = string.Empty;
                        rs.LastPurchaseDate = string.Empty;
                        rs.LastPurchaseType = string.Empty;
                        rs.LastActivation = string.Empty;
                        rs.PurchaseCount = 0;
                    }

                    //unsure why but wouldn't work when hard coding aircoach string so had to usevaraible which allowed it to save

                    string aircoach = "AirCoach";

                    //Take 6 Associated Organisations, we have 5 org fields but we will have to remove "First Bus" anyway...
                    
                    var myOrgs = JArray.Parse(u["AssociatedOrganisations"].ToJson()).ToObject<List<string>>().Where(o => o.Contains("First Bus") || o.Contains(aircoach)).ToList();
                    rs.Source = "Please update with new source";

                    string orgName = "";



                    if (myOrgs.Contains("First Bus"))
                    {
                        rs.Source = "MTicket";
                        orgName = "FirstBus";
                        myOrgs.RemoveAll(str => str.Equals("First Bus"));
                    }

                    if (myOrgs.Contains(aircoach))
                    {
                        rs.Source = aircoach;
                        orgName = "Aircoach";
                        myOrgs.RemoveAll(str => str.Equals(aircoach));
                    }

                    //MARKETING LOGIC
                    /* GOBRIEN GDPR Changes*/
                    string marketingPrefs = GetUsersMarketingPrefs(u, orgName);

                    if (!string.IsNullOrEmpty(marketingPrefs))
                    {
                        rs.YesPlease = marketingPrefs;
                        rs.NoThanks = false;
                        rs.MarketingOptInDate = ConvertDate(_startDt);
                    }
                    else
                    {
                        rs.NoThanks = true;
                        rs.YesPlease = string.Empty;
                        rs.MarketingOptInDate = string.Empty;
                    }

                    bool age16 = u.Contains("tags") && u["tags"].ToBsonDocument().Contains("Details_" + orgName + "_MarketingOptInDate_IsChecked_AgeOver16") &&
                    u["tags"]["Details_" + orgName + "_MarketingOptInDate_IsChecked_AgeOver16"].ToString() == "1" ? true : false;

                    if (age16)
                        rs.Age16 = true;
                    else
                        rs.Age16 = false;

                    rs.NewPreference = true;

                    string strOrgs = string.Empty;

                    foreach (var myOrg in myOrgs.Take(5))
                    {
                        //Does conversion so it is more utf-8 complaint
                        strOrgs += myOrg.Replace("–", "-") + "|";
                    }

                    if (myOrgs.Count() > 0 && !string.IsNullOrEmpty(strOrgs))
                    {
                        string[] organisations = strOrgs.Split(new[] { '|' }, 5, StringSplitOptions.RemoveEmptyEntries);

                        if (!string.IsNullOrEmpty(organisations[0]))
                        {
                            //org0
                            rs.Organisation1 = organisations[0].TrimEnd('|');
                        }
                        else
                            rs.Organisation1 = string.Empty;

                        if (organisations.Count() > 1)
                        {
                            //org1
                            if (!string.IsNullOrEmpty(organisations[1]))
                                rs.Organisation2 = organisations[1].TrimEnd('|');
                            else
                                rs.Organisation2 = string.Empty;
                        }
                        else rs.Organisation2 = string.Empty;


                        if (organisations.Count() > 2)
                        {
                            //org2
                            if (!string.IsNullOrEmpty(organisations[2]))
                                rs.Organisation3 = organisations[2].TrimEnd('|');
                            else
                                rs.Organisation3 = string.Empty;
                        }
                        else rs.Organisation3 = string.Empty;

                        if (organisations.Count() > 3)
                        {
                            //org3
                            if (!string.IsNullOrEmpty(organisations[3]))
                                rs.Organisation4 = organisations[3].TrimEnd('|');
                            else
                                rs.Organisation4 = string.Empty;
                        }
                        else rs.Organisation4 = string.Empty;

                        if (organisations.Count() > 4)
                        {
                            //org4
                            if (!string.IsNullOrEmpty(organisations[4]))
                                rs.Organisation5 = organisations[4].TrimEnd('|');
                            else
                                rs.Organisation5 = string.Empty;
                        }
                        else rs.Organisation5 = string.Empty;
                    }
                    else
                    {
                        rs.Organisation1 = string.Empty;
                        rs.Organisation2 = string.Empty;
                        rs.Organisation3 = string.Empty;
                        rs.Organisation4 = string.Empty;
                        rs.Organisation5 = string.Empty;
                    }


                    rs.LastDeviceId = u.Contains("tags") && u["tags"].ToBsonDocument().Contains("LastDeviceID") ? u["tags"]["LastDeviceID"].ToString() : string.Empty;

                    sw.WriteLine(string.Join(",",
                                            rs.AccountId,
                                            rs.UserId,
                                            rs.FirstName.Replace(",", string.Empty),
                                            rs.LastName.Replace(",", string.Empty),
                                            rs.Email.Replace(",", string.Empty),
                                            rs.Address.Replace(",", string.Empty),
                                            rs.Town.Replace(",", string.Empty),
                                            rs.County.Replace(",", string.Empty),
                                            rs.Country.Replace(",", string.Empty),
                                            rs.Postcode.Replace(",", string.Empty),
                                            rs.PhoneNumber.Replace(",", string.Empty),
                                            rs.Created,
                                            rs.Updated,
                                            rs.MarketingOptInDate,
                                            rs.NewPreference,
                                            rs.YesPlease,
                                            rs.NoThanks,
                                            rs.Age16,
                                            //rs.MarketingOptIn,
                                            rs.Organisation1,
                                            rs.Organisation2,
                                            rs.Organisation3,
                                            rs.Organisation4,
                                            rs.Organisation5,
                                            rs.FirstPurchaseDate,
                                            rs.FirstPurchaseType,
                                            rs.LastPurchaseDate,
                                            rs.LastPurchaseType,
                                            rs.LastActivation,
                                            rs.PurchaseCount,
                                            rs.Source,
                                            rs.LastDeviceId));
                });

                sw.Flush();
                _streamLength = s.Length;
            }
            return s;
        }
        private void InitialiseUsersCRM(IEnumerable<string> orgs, DateTime startDt, DateTime endDt)
        {
            _orgs = orgs;
            _startDt = startDt;
            _endDt = endDt;
        }
        private void GetUserDataUsersCRM()
        {
            BsonDocument createdUsersQuery = GetUsersCreatedQuery(_orgs, _startDt.ToString(), _endDt.ToString());
            //Get the id of all users created in the reporting period.
            _createdUsers = QueryDatabaseExistingConnection(mongoDatabaseClient, "Users", createdUsersQuery).Where(u => !u["Email"].ToString().Contains("@mailinator.com")).Select(u => u["_id"].ToString()).Distinct().ToList();
            BsonDocument marketingUsersQuery = GetMarketingChangeQuery(_orgs, _startDt.ToString(), _endDt.ToString());
            //Get the id of all users who changed marketing prefs in the reporting period.

            _marketingUsers = QueryDatabaseExistingConnection(mongoDatabaseClient, "Log_Activity", marketingUsersQuery).Where(activity => !activity["User"]["Email"].ToString().Contains("@mailinator.com")).Select(activity => activity["User"]["ID"].ToString()).Distinct().ToList();
            _users = new List<string>();

            //for each user created on this day, add the user id to the _users collection.
            foreach(string cu in _createdUsers)
            {
                _users.Add(cu);
            }

            //for each user who changed their marketing prefs on this day, only add them to the _users collection if they are not already there. i.e don't add duplicate user id's to the users collection.

            foreach(string mu in _marketingUsers)
            {
                if (!_users.Contains(mu))
                    _users.Add(mu);
            }

        }

        private Dictionary<string, MappingRule<ReportStructUsersCRM>> MapDataUsersCRM()
        {
            var dictMap = new Dictionary<string, MappingRule<ReportStructUsersCRM>>() {
                { "AccountId", r => r.AccountId},
                { "UserId", r => r.UserId },
                { "FirstName", r => r.FirstName.Replace(",",string.Empty) },
                { "LastName", r => r.LastName.Replace(",",string.Empty)},
                { "Email", r => r.Email.Replace(",",string.Empty) },
                { "Address", r => r.Address.Replace(",",string.Empty)},
                { "Town", r => r.Town.Replace(",",string.Empty)},
                { "County", r => r.County.Replace(",",string.Empty)},
                { "Country", r => r.Country.Replace(",",string.Empty)},
                { "Postcode", r => r.Postcode.Replace(",",string.Empty)},
                { "PhoneNumber", r => r.PhoneNumber.Replace(",",string.Empty)},
                { "Created", r => r.Created},
                { "Updated", r => r.Updated},
                { "YesPlease", r => r.YesPlease},
                { "NoThanks", r => r.NoThanks.ToString()},
                { "Age16", r => r.Age16.ToString()},
                { "MarketingOptInDate", r => string.IsNullOrEmpty(r.MarketingOptInDate.ToString()) ? string.Empty : r.MarketingOptInDate.ToString()},
                { "NewPreference", r => r.NewPreference.ToString()},
                { "Organisation1", r => r.Organisation1},
                { "Organisation2", r => r.Organisation2},
                { "Organisation3", r => r.Organisation3},
                { "Organisation4", r => r.Organisation4},
                { "Organisation5", r => r.Organisation5},
                { "FirstPurchaseDate", r => r.FirstPurchaseDate},
                { "FirstPurchaseType", r => r.FirstPurchaseType.Replace(",",string.Empty)},
                { "LastPurchaseDate", r => r.LastPurchaseDate},
                { "LastPurchaseType", r => r.LastPurchaseType.Replace(",",string.Empty)},
                { "LastActivation", r => r.LastActivation},
                { "PurchaseCount", r => r.PurchaseCount.ToString()},
                { "Source", r => r.Source},
                { "LastDeviceId", r => r.LastDeviceId}
            };
            return dictMap;
        }
        public struct ReportStructUsersCRM
        {
            public string AccountId;
            public string UserId;
            public string FirstName;
            public string LastName;
            public string Email;
            public string Address;
            public string Town;
            public string County;
            public string Country;
            public string Postcode;
            public string PhoneNumber;
            public string Created;
            public string Updated;
            public string YesPlease;
            public bool NoThanks;
            public bool Age16;
            public string MarketingOptInDate;
            public bool NewPreference;
            public string Organisation1;
            public string Organisation2;
            public string Organisation3;
            public string Organisation4;
            public string Organisation5;
            public string FirstPurchaseDate;
            public string FirstPurchaseType;
            public string LastPurchaseDate;
            public string LastPurchaseType;
            public string LastActivation;
            public int PurchaseCount;
            public string Source;
            public string LastDeviceId;

        }

        private BsonDocument GetUsersCreatedQuery(IEnumerable<string> orgs, string startDate = "", string endDate = "")
        {
            BsonDocument query = new BsonDocument();

            query.Add("AssociatedOrganisations", new BsonDocument("$in", includeOrgs()));

            // date filtering
            if (!string.IsNullOrWhiteSpace(startDate) && !string.IsNullOrWhiteSpace(endDate))
            {
                var start = DateTime.Parse(startDate);
                var end = DateTime.Parse(endDate);
                var created = new BsonDocument("Created", new BsonDocument().Add("$gte", start).Add("$lte", end));

                query.Add(created);
            }
            return query;
        }
        private BsonDocument GetMarketingChangeQuery(IEnumerable<string> orgs, string startDate = "", string endDate = "")
        {
            BsonDocument query = new BsonDocument();

            query.Add("Organisation", new BsonDocument("$in", includeOrgs()));

            // date filtering
            if (!string.IsNullOrWhiteSpace(startDate) && !string.IsNullOrWhiteSpace(endDate))
            {
                var start = DateTime.Parse(startDate);
                var end = DateTime.Parse(endDate);
                var DT = new BsonDocument("DT", new BsonDocument().Add("$gte", start).Add("$lte", end));
                query.Add(DT);

                var optOut = new BsonDocument("ActivityTypeID", cstrActivityTypeID_OptOut);
                var optIn = new BsonDocument("ActivityTypeID", cstrActivityTypeID_OptIn);

                var bsonArr = new BsonArray() { optIn, optOut };
                query.Add("$or", bsonArr);
            }
            return query;
        }
        private string GetUsersMarketingPrefs(BsonDocument user, string Org)
        {
            char[] charToTrim = { ';' };

            if (user == null || !user.Contains("tags")) return string.Empty;

            string marketingPrefs = string.Empty;

            bool optOutAll = user["tags"].ToBsonDocument().Contains("Details_" + Org + "_MarketingOptInDate_IsChecked_OptOutAll")
                && user["tags"]["Details_" + Org + "_MarketingOptInDate_IsChecked_OptOutAll"].ToString() == "1" ? true : false;

            if (optOutAll) return string.Empty;

            bool email = user["tags"].ToBsonDocument().Contains("Details_" + Org + "_MarketingOptInDate_IsChecked_Email")
                && user["tags"]["Details_" + Org + "_MarketingOptInDate_IsChecked_Email"].ToString() == "1" ? true : false;

            if (email) marketingPrefs = "Email/Electronic Means;";

            bool sms = user["tags"].ToBsonDocument().Contains("Details_" + Org + "_MarketingOptInDate_IsChecked_SMS")
                && user["tags"]["Details_" + Org + "_MarketingOptInDate_IsChecked_SMS"].ToString() == "1" ? true : false;

            if (sms) marketingPrefs += "SMS;";

            bool phone = user["tags"].ToBsonDocument().Contains("Details_" + Org + "_MarketingOptInDate_IsChecked_Phone")
                && user["tags"]["Details_" + Org + "_MarketingOptInDate_IsChecked_Phone"].ToString() == "1" ? true : false;

            if (phone) marketingPrefs += "Phone;";

            bool post = user["tags"].ToBsonDocument().Contains("Details_" + Org + "_MarketingOptInDate_IsChecked_Post")
                && user["tags"]["Details_" + Org + "_MarketingOptInDate_IsChecked_Post"].ToString() == "1" ? true : false;

            if (post) marketingPrefs += "Post;";

            marketingPrefs = marketingPrefs.TrimEnd(charToTrim);

            return marketingPrefs;
        }

        #endregion

        #region TicketPurchases

        BsonDocument user = null;
        Dictionary<string, List<string>> MappingList = new Dictionary<string, List<string>>();

        public MemoryStream TicketPurchases(IEnumerable<string> orgs, DateTime startDt, DateTime endDt)
        {
            InitialiseTicketPurchasesCRM(orgs, startDt, endDt);
            GenerateDataTicketPurchasesCRM();
            return GenerateMemoryStreamTicketPurchasesCRM("Processing First Bus Ticket Purchases");
        }
        private void GenerateDataTicketPurchasesCRM()
        {

            BsonDocument activityQuery = GetActivityQueryTicketPurchasesCRM(_startDt.ToString(), _endDt.ToString());
            activities = QueryDatabaseExistingConnection(mongoDatabaseClient, "Log_Activity", activityQuery).ToList();

            var mappingQuery = GetAggregationQueryTicketPurchasesCRM(_startDt, _endDt, _orgs);
            var mappings = QueryDatabaseAggregateExistingConnection(mongoDatabaseClient, "Log_Activity", mappingQuery);

            foreach(BsonDocument m in mappings)
            {
                if (m.Contains("Reference"))
                {
                    string strKey = m["Reference"].AsString;
                    string strValue = m["_id"].AsString;
                    if (!MappingList.ContainsKey(strKey))
                    {
                        MappingList[strKey] = new List<string>();
                    }
                    MappingList[strKey].Add(strValue);
                }
            }
        }
        private MemoryStream GenerateMemoryStreamTicketPurchasesCRM(string name)
        {
            if (!activities.Any())
                return new MemoryStream();

            ReportStructTicketPurchasesCRM rs = new ReportStructTicketPurchasesCRM();
            var dictMap = MapDataTicketPurchasesCRM();
            var s = new MemoryStream();
            using (var sw = new StreamWriter(s))
            {
                sw.WriteLine(
                    @"ActivityID,PurchaseActivityID,Device ID,Shared Ticket ID,Activity,Ticket_Category,Ticket_ProductID,Price,Ticket_Expiry,Ticket_Activated");

                foreach(BsonDocument u in activities)
                {
                    rs.ActivityID = u["_id"].ToString();

                    var reference = u.Contains("MetaData") && u["MetaData"].ToBsonDocument().Contains("Reference") ? u["MetaData"]["Reference"].ToString() : string.Empty;

                    if (!string.IsNullOrEmpty(reference))
                    {
                        if (MappingList.ContainsKey(reference))
                        {
                            List<string> strVals;

                            MappingList.TryGetValue(reference, out strVals);

                            rs.PurchaseActivityID = strVals.FirstOrDefault();
                        }
                        else
                        {
                            rs.PurchaseActivityID = reference;
                        }
                    }
                    else
                    {
                        rs.PurchaseActivityID = string.Empty;
                    }

                    rs.DeviceID = u["MetaData"].ToBsonDocument().Contains("Ticket_DeviceID") ? u["MetaData"]["Ticket_DeviceID"].ToString() : string.Empty;
                    rs.SharedTicketID = string.Empty;

                    var shareTicketID = u["MetaData"].ToBsonDocument().Contains("Ticket_ExpiryShareID") ? u["MetaData"]["Ticket_ExpiryShareID"].ToString() : string.Empty;

                    if (!string.IsNullOrEmpty(shareTicketID))
                    {
                        BsonDocument sharedTicketQuery = new BsonDocument("_id", shareTicketID);
                        var tickets = QueryDatabaseExistingConnection(mongoDatabaseClient, "Log_Activity", sharedTicketQuery);
                        var ticket = tickets.First();
                        DateTime ticketCreated;
                        var ticketCreatedString = ticket["DT"].ToString();

                        string[] originalTicketIds = ticket["MetaData"]["OriginalTicketIDs"].ToString().Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                        string[] ticketIds = ticket["MetaData"]["TicketIDs"].ToString().Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                        int index = Array.IndexOf(originalTicketIds, u["_id"].ToString());

                        DateTime.TryParse(ticketCreatedString, out ticketCreated);

                        if (ticketCreated.Date == DateTime.Parse(u["DT"].ToString()).Date)
                        {
                            BsonDocument newTicketQuery = new BsonDocument("_id", ticketIds[index]);
                            var newTicket = QueryDatabaseExistingConnection(mongoDatabaseClient, "Log_Activity", newTicketQuery).FirstOrDefault();

                            if (newTicket != null && newTicket["MetaData"].ToBsonDocument().Contains("Claimed"))
                            {
                                rs.SharedTicketID = newTicket.Contains("_id") ? newTicket["_id"].ToString() : string.Empty;
                            }
                            else
                            {
                                rs.SharedTicketID = string.Empty;
                                shareTicketID = string.Empty;
                            }
                        }
                        else
                        {
                            rs.SharedTicketID = string.Empty;
                            shareTicketID = string.Empty;
                        }
                    }
                    else
                    {
                        rs.SharedTicketID = string.Empty;
                    }

                    rs.Datetime = ConvertDate(DateTime.Parse(u["DT"].ToString()));
                    rs.Activity = u["Name"].ToString();
                    //Convert n dash to regular one to make it utf-8 complaint
                    //Can't do at source right now as this requires massive regression test
                    rs.Organisation = u["Organisation"].ToString().Replace("–", "-");
                    if (u.Contains("User")) rs.UserID = u["User"].ToBsonDocument()["ID"].ToString();
                    try
                    {
                        if (u["User"].ToBsonDocument().Contains("ID") && u["User"].ToBsonDocument().Contains("Name") && !u["User"]["Name"].ToString().Equals("Guest User")) user = QueryDatabaseExistingConnection(mongoDatabaseClient, "Users", new BsonDocument().Add("_id", u["User"].ToBsonDocument()["ID"].ToString())).First();
                        else if (u["User"].ToBsonDocument().Contains("Name") && u["User"]["Name"].ToString().Equals("Guest User")) user = null;
                    }
                    catch(Exception e)
                    {

                    }
                    if (user != null)
                        rs.Postcode = user.Contains("tags") ? (user["tags"].ToBsonDocument().Contains("Details_Postcode") ? user["tags"]["Details_Postcode"].ToString() : string.Empty) : string.Empty;
                    else
                        rs.Postcode = string.Empty;

                    rs.Reference = u["MetaData"].ToBsonDocument().Contains("Reference") ? u["MetaData"]["Reference"].ToString() : string.Empty;
                    rs.TicketCategory = u["MetaData"].ToBsonDocument().Contains("Ticket_Category") ? u["MetaData"]["Ticket_Category"].ToString() : string.Empty;
                    rs.TicketProductID = u["MetaData"].ToBsonDocument().Contains("Ticket_ProductID") ? u["MetaData"]["Ticket_ProductID"].ToString() : string.Empty;
                    rs.TicketTimeout = (u["MetaData"].ToBsonDocument().Contains("Ticket_Timeout") && !string.IsNullOrEmpty(u["MetaData"]["Ticket_Timeout"].ToString())) ? ConvertDate(DateTime.Parse(u["MetaData"]["Ticket_Timeout"].ToString())) : string.Empty;
                    rs.Price = u["MetaData"].ToBsonDocument().Contains("Price") ? u["MetaData"]["Price"].ToString() : string.Empty;
                    rs.PriceCarnet = u["MetaData"].ToBsonDocument().Contains("Price_Carnet") ? u["MetaData"]["Price_Carnet"].ToString() : string.Empty;
                    rs.TicketInstance = u["MetaData"].ToBsonDocument().Contains("Ticket_Instance") ? u["MetaData"]["Ticket_Instance"].ToString() : string.Empty;
                    rs.UserAgent = u["MetaData"].ToBsonDocument().Contains("User-Agent") ? u["MetaData"]["User-Agent"].ToString() : string.Empty;
                    rs.TicketFirstFetched = (u["MetaData"].ToBsonDocument().Contains("Ticket_FirstFetched") && !string.IsNullOrEmpty(u["MetaData"]["Ticket_FirstFetched"].ToString())) ? ConvertDate(DateTime.Parse(u["MetaData"]["Ticket_FirstFetched"].ToString())) : string.Empty;
                    rs.TicketExpiry = (u["MetaData"].ToBsonDocument().Contains("Ticket_Expiry") && !string.IsNullOrEmpty(u["MetaData"]["Ticket_Expiry"].ToString())) ? ConvertDate(DateTime.Parse(u["MetaData"]["Ticket_Expiry"].ToString())) : string.Empty;
                    rs.TicketActivated = (u["MetaData"].ToBsonDocument().Contains("Ticket_Activated") && !string.IsNullOrEmpty(u["MetaData"]["Ticket_Activated"].ToString())) ? ConvertDate(DateTime.Parse(u["MetaData"]["Ticket_Activated"].ToString())) : string.Empty;
                    rs.PriceWaived = u["MetaData"].ToBsonDocument().Contains("Ticket_PriceWaived") ? u["MetaData"]["Ticket_PriceWaived"].ToString() : string.Empty;
                    rs.TicketUnlockedBy = u["MetaData"].ToBsonDocument().Contains("Ticket_UnlockedBy") ? u["MetaData"]["Ticket_UnlockedBy"].ToString() : string.Empty;
                    rs.TicketLockedVersionID = u["MetaData"].ToBsonDocument().Contains("Ticket_LockedVersionID") ? u["MetaData"]["Ticket_LockedVersionID"].ToString() : string.Empty;
                    rs.TicketAutoActivateDate = (u["MetaData"].ToBsonDocument().Contains("Ticket_AutoActivateDate") && !string.IsNullOrEmpty(u["MetaData"]["Ticket_AutoActivateDate"].ToString())) ? ConvertDate(DateTime.Parse(u["MetaData"]["Ticket_AutoActivateDate"].ToString())) : string.Empty;
                    rs.TicketAutoActivate = u["MetaData"].ToBsonDocument().Contains("Ticket_AutoActivate") ? u["MetaData"]["Ticket_AutoActivate"].ToString() : string.Empty;
                    rs.TicketForceExpiredBy = u["MetaData"].ToBsonDocument().Contains("Ticket_ForceExpiredBy") ? u["MetaData"]["Ticket_ForceExpiredBy"].ToString() : string.Empty;
                    rs.TicketForceExpiredReason = u["MetaData"].ToBsonDocument().Contains("Ticket_ExpiryReason") ? u["MetaData"]["Ticket_ExpiryReason"].ToString() : string.Empty;
                    rs.TicketExpiryReason = u["MetaData"].ToBsonDocument().Contains("Ticket_ExpiryReason") ? u["MetaData"]["Ticket_ExpiryReason"].ToString() : string.Empty;
                    rs.TicketExpiryReasonDetail = u["MetaData"].ToBsonDocument().Contains("Ticket_ExpiryReasonDetail") ? u["MetaData"]["Ticket_ExpiryReasonDetail"].ToString() : string.Empty;

                    sw.WriteLine(string.Join(",",
                                             rs.ActivityID,
                                             rs.PurchaseActivityID,
                                             rs.DeviceID,
                                             rs.SharedTicketID,
                                             rs.Activity.Replace(",", string.Empty).Replace("#", string.Empty).Replace("¬", string.Empty).Replace("–", "-"),
                                             rs.TicketCategory.Replace(",", string.Empty).Replace("#", string.Empty).Replace("¬", string.Empty).Replace("–", "-"),
                                             rs.TicketProductID,
                                             rs.Price,
                                             rs.TicketExpiry,
                                             rs.TicketActivated
                                            ));
                }

                sw.Flush();
                _streamLength = s.Length;
            }
            return s;
        }

        private void InitialiseTicketPurchasesCRM(IEnumerable<string> orgs, DateTime startDt, DateTime endDt)
        {
            _orgs = orgs;
            _startDt = startDt;
            _endDt = endDt;
        }
        private BsonDocument GetActivityQueryTicketPurchasesCRM(string startDate = "", string endDate = "")
        {
            BsonDocument query = new BsonDocument();

            query.Add("Organisation", new BsonDocument("$in", includeOrgs()));
            query.Add("ActivityTypeID", cstrActivityTypeID_Tickets);

            // date filtering
            if (!string.IsNullOrWhiteSpace(startDate) && !string.IsNullOrWhiteSpace(endDate))
            {
                var start = DateTime.Parse(startDate);
                var end = DateTime.Parse(endDate);

                query.Add("DT", new BsonDocument().Add("$gte", start).Add("$lte", end));
            }
            return query;
        }

        private Dictionary<string, MappingRule<ReportStructTicketPurchasesCRM>> MapDataTicketPurchasesCRM()
        {
            var dictMap = new Dictionary<string, MappingRule<ReportStructTicketPurchasesCRM>>() {
                { "ActivityID", r => r.ActivityID},
                { "PurchaseActivityID", r => r.PurchaseActivityID},
                { "DeviceID", r => r.DeviceID },
                { "SharedTicketID", r => r.SharedTicketID },
                { "Datetime", r => r.Datetime},
                { "Activity", r => r.Activity.Replace(",",string.Empty).Replace("#",string.Empty).Replace("¬",string.Empty)},
                { "Organisation", r => r.Organisation.Replace(",",string.Empty).Replace("#",string.Empty).Replace("¬",string.Empty)},
                { "UserID", r => r.UserID},
                { "Postcode", r => r.Postcode.Replace(",",string.Empty).Replace("#",string.Empty).Replace("¬",string.Empty)},
                { "Reference", r => r.Reference},
                { "TicketCategory", r => r.TicketCategory.Replace(",",string.Empty).Replace("#",string.Empty).Replace("¬",string.Empty)},
                { "TicketProductID", r => r.TicketProductID},
                { "TicketTimeout", r => r.TicketTimeout},
                { "Price", r => r.Price},
                { "PriceCarnet", r => r.PriceCarnet},
                { "UserAgent", r => r.UserAgent},
                { "TicketFirstFetched", r => r.TicketFirstFetched},
                { "TicketExpiry", r => r.TicketExpiry},
                { "TicketActivated", r => r.TicketActivated},
                { "PriceWaived", r => r.PriceWaived},
                { "TicketUnlockedBy", r => r.TicketUnlockedBy},
                { "TicketLockedVersionID", r => r.TicketLockedVersionID},
                { "TicketAutoActivateDate", r => r.TicketAutoActivateDate},
                { "TicketAutoActivate", r => r.TicketAutoActivate},
                { "TicketForceExpiredBy", r => r.TicketForceExpiredBy},
                { "TicketForceExpiredReason", r => r.TicketForceExpiredReason.Replace(",",string.Empty).Replace("#",string.Empty).Replace("¬",string.Empty)},
                { "TicketExpiryReason", r => r.TicketExpiryReason.Replace(",",string.Empty).Replace("#",string.Empty).Replace("¬",string.Empty)},
                { "TicketExpiryReasonDetail", r => r.TicketExpiryReasonDetail.Replace(",",string.Empty).Replace("#",string.Empty).Replace("¬",string.Empty)}
            };
            return dictMap;
        }
        public struct ReportStructTicketPurchasesCRM
        {
            public string ActivityID;
            public string PurchaseActivityID;
            public string DeviceID;
            public string SharedTicketID;
            public string Datetime;
            public string Activity;
            public string Organisation;
            public string UserID;
            public string Postcode;
            public string Reference;
            public string TicketCategory;
            public string TicketProductID;
            public string TicketTimeout;
            public string Price;
            public string PriceCarnet;
            public string TicketInstance;
            public string UserAgent;
            public string TicketFirstFetched;
            public string TicketExpiry;
            public string TicketActivated;
            public string PriceWaived;
            public string TicketUnlockedBy;
            public string TicketLockedVersionID;
            public string TicketAutoActivateDate;
            public string TicketAutoActivate;
            public string TicketForceExpiredBy;
            public string TicketForceExpiredReason;
            public string TicketExpiryReason;
            public string TicketExpiryReasonDetail;
        }

        private BsonDocument[] GetAggregationQueryTicketPurchasesCRM(DateTime start, DateTime end, IEnumerable<string> orgs)
        {
            BsonDocument query = new BsonDocument();

            var match = new BsonDocument("$match",
               new BsonDocument("DT", new BsonDocument()
               {
           { "$gte", start},
           { "$lte", end},
               }).Add("Organisation", new BsonDocument("$in", new BsonArray(orgs))
               ).Add("ActivityTypeID", "563cc9ed-9fe3-4fcd-bf79-b10bdcaefac6"));
            //activity, dt, org for index purposes.

            var project = new BsonDocument("$project",
                new BsonDocument()
                {
           { "_id", "$_id"},
           {"Reference", "$MetaData.Reference"}
                });


            var queryToReturn = new BsonDocument[] { match, project };

            return queryToReturn;
        }

        #endregion

        #region PurchaseTransactions

        const string cstrActivityTypeID_SuccessfulTransactions = "563cc9ed-9fe3-4fcd-bf79-b10bdcaefac6";
        const string cstrActivityTypeID_PromoCodeIssued = "bcViJOeJxgeq";

        Dictionary<string, string> PromoCodes = new Dictionary<string, string>();

        public MemoryStream PurchaseTransactions(IEnumerable<string> orgs, DateTime startDt, DateTime endDt)
        {
            InitialisePurchaseTransactionsCRM(orgs, startDt, endDt);
            GenerateDataPurchaseTransactionsCRM();
            return GenerateMemoryStreamPurchaseTransactionsCRM("Processing First Bus Transactions");
        }
        private void GenerateDataPurchaseTransactionsCRM()
        {

            BsonDocument ticket_query = GetActivityQueryPurchaseTransactionsCRM(_orgs, _startDt.ToString(), _endDt.ToString(), cstrActivityTypeID_SuccessfulTransactions);
            activities = QueryDatabaseExistingConnection(mongoDatabaseClient, "Log_Activity", ticket_query).Where(a => !a["User"]["Email"].ToString().Contains("@mailinator.com"));

            //get promo codes
            BsonDocument promo_query = GetActivityQueryPurchaseTransactionsCRM(_orgs, _startDt.ToString(), _endDt.ToString(), cstrActivityTypeID_PromoCodeIssued);
            var promoCodes = QueryDatabaseExistingConnection(mongoDatabaseClient, "Log_Activity", promo_query);

            foreach(BsonDocument promoCode in promoCodes)
            {
                if (!PromoCodes.ContainsKey(promoCode["MetaData"]["Reference"].ToString()))
                {
                    PromoCodes.Add(promoCode["MetaData"]["Reference"].ToString(), promoCode["MetaData"]["PromotionalCode"].ToString());
                }
            }
        }
        private MemoryStream GenerateMemoryStreamPurchaseTransactionsCRM(string name)
        {
            ReportStructPurchaseTransactionsCRM rs = new ReportStructPurchaseTransactionsCRM();
            var dictMap = MapDataPurchaseTransactionsCRM();
            var s = new MemoryStream();
            using (var sw = new StreamWriter(s))
            {
                sw.WriteLine(@"PurchaseActivityID,User ID,DateTimeUTC,Organisation,Price,Promo Code,PaymentProvider,Reference,Source,Device Id");

                foreach(BsonDocument u in activities)
                {
                    //ACCOUNT ID SUBJECT TO CHANGE
                    rs.PurchaseActivityID = u["_id"].ToString();
                    rs.UserID = u["User"].ToBsonDocument()["ID"].ToString();
                    rs.DateTimeUTC = ConvertDate(DateTime.Parse(u["DT"].ToString()));
                    //Convert n dash to regular one to make it utf-8 complaint
                    //Can't do at source right now as this requires massive regression test
                    rs.Organisation = u["Organisation"].ToString().Replace("–", "-");
                    rs.Price = u["MetaData"].ToBsonDocument().Contains("Price") ? u["MetaData"]["Price"].ToString() : string.Empty;

                    if (PromoCodes.ContainsKey(u["MetaData"].ToBsonDocument().Contains("Reference") ? u["MetaData"]["Reference"].ToString() : string.Empty))
                    {
                        string promo = string.Empty;
                        bool val = PromoCodes.TryGetValue(u["MetaData"]["Reference"].ToString(), out promo);
                        rs.PromoCode = promo;
                    }
                    else
                        rs.PromoCode = string.Empty;

                    rs.PaymentProvider = u["MetaData"].ToBsonDocument().Contains("PaymentProvider") ? u["MetaData"]["PaymentProvider"].ToString() : string.Empty;
                    rs.Reference = u["MetaData"].ToBsonDocument().Contains("Reference") ? u["MetaData"]["Reference"].ToString() : u["_id"].ToString();
                    rs.Source = "MTicket";
                    rs.DeviceID = u["MetaData"].ToBsonDocument().Contains("DeviceID") ? u["MetaData"]["DeviceID"].ToString() : string.Empty;

                    sw.WriteLine(string.Join(",",
                                             rs.PurchaseActivityID,
                                             rs.UserID,
                                             rs.DateTimeUTC,
                                             rs.Organisation,
                                             rs.Price,
                                             rs.PromoCode,
                                             rs.PaymentProvider,
                                             rs.Reference,
                                             rs.Source,
                                             rs.DeviceID));
                }

                sw.Flush();
                _streamLength = s.Length;
            }
            return s;
        }

        private void InitialisePurchaseTransactionsCRM(IEnumerable<string> orgs, DateTime startDt, DateTime endDt)
        {
            _orgs = orgs;
            _startDt = startDt;
            _endDt = endDt;
        }

        private BsonDocument GetActivityQueryPurchaseTransactionsCRM(IEnumerable<string> orgs, string startDate = "", string endDate = "", string ActivityTypeId = "")
        {
            BsonDocument query = new BsonDocument();

            query.Add("Organisation", new BsonDocument("$in", includeOrgs()));
            query.Add("ActivityTypeID", ActivityTypeId);

            // date filtering
            if (!string.IsNullOrWhiteSpace(startDate) && !string.IsNullOrWhiteSpace(endDate))
            {
                var start = DateTime.Parse(startDate);
                var end = DateTime.Parse(endDate);

                query.Add("DT", new BsonDocument().Add("$gte", start).Add("$lte", end));
            }
            return query;
        }

        private Dictionary<string, MappingRule<ReportStructPurchaseTransactionsCRM>> MapDataPurchaseTransactionsCRM()
        {
            var dictMap = new Dictionary<string, MappingRule<ReportStructPurchaseTransactionsCRM>>() {
                { "PurchaseActivityID", r => r.PurchaseActivityID},
                { "UserID", r => r.DeviceID.Replace(",",string.Empty) },
                { "DateTimeUTC", r => r.DateTimeUTC },
                { "Organisation", r => r.Organisation.Replace(",",string.Empty)},
                { "Price", r => r.Price},
                { "PromoCode", r => r.PromoCode},
                { "PaymentProvider", r => r.PaymentProvider.Replace(",",string.Empty)},
                { "Reference", r => r.Reference},
                { "Source", r => r.Source},
                { "DeviceID", r => r.DeviceID}
            };
            return dictMap;
        }
        public struct ReportStructPurchaseTransactionsCRM
        {
            public string PurchaseActivityID;
            public string UserID;
            public string DateTimeUTC;
            public string Organisation;
            public string Price;
            public string PromoCode;
            public string PaymentProvider;
            public string Reference;
            public string Source;
            public string DeviceID;
        }

        #endregion

        #region generalUtil

        delegate string MappingRule<T>(T value);
        List<BsonDocument> users;
        IEnumerable<BsonDocument> activities;
        long _streamLength = 0;
        const string cstrActivityTypeID_Tickets = "79b9fa2c-56f8-403d-aed1-8e1d380b1af0";

        public string Log
        {
            get { return _log; }
            private set { _log += DateTime.UtcNow.ToString("yyyy/MM/dd hh:mm:ss.sssZ") + ": " + value + "<br />"; }
        }

        private IEnumerable<BsonDocument> QueryDatabaseNewConnection(string collectionName, BsonDocument query)
        {
            string connectionString = "mongodb://ce-ro:zFY6LTEG4DgcAf3D@coreengine-shard-00-03-x2akk.mongodb.net:27017/admin?ssl=true&readPreference=secondary&connectTimeoutMS=10000&authSource=admin&authMechanism=SCRAM-SHA-1";

            MongoClient client = null;
            IMongoDatabase db = null;
            IMongoCollection<BsonDocument> collection = null;
            IEnumerable<BsonDocument> result = null;
            try
            {
                client = new MongoClient(connectionString);
                db = client.GetDatabase("Corethree");
                collection = db.GetCollection<BsonDocument>(collectionName);
                result = collection.Find(query).ToEnumerable();
            }
            catch (Exception e)
            {
                throw e;
            }

            return result;
        }

        private IEnumerable<BsonDocument> QueryDatabaseExistingConnection(MongoClient client, string collectionName, BsonDocument query)
        {
            IMongoDatabase db = null;
            IMongoCollection<BsonDocument> collection = null;
            IEnumerable<BsonDocument> result = null;
            try
            {
                db = client.GetDatabase("Corethree");
                collection = db.GetCollection<BsonDocument>(collectionName);
                result = collection.Find(query).ToEnumerable();
            }
            catch (Exception e)
            {
                return QueryDatabaseNewConnection(collectionName, query);
            }

            return result;
        }

        private IEnumerable<BsonDocument> QueryDatabaseAggregateExistingConnection(MongoClient client, string collectionName, BsonDocument[] aggregate)
        {
            
            IMongoDatabase db = null;
            IMongoCollection<BsonDocument> collection = null;
            IEnumerable<BsonDocument> result = null;
            try
            {
                db = client.GetDatabase("Corethree");
                collection = db.GetCollection<BsonDocument>(collectionName);
                result = collection.Aggregate<BsonDocument>(aggregate).ToList();
            }
            catch (Exception e)
            {
                throw e;
            }

            return result;
        }

        public string GetUserAgentParts(string userAgent, string part)
        {
            if (userAgent.ToLower().Contains("http") || string.IsNullOrEmpty(userAgent))
                return userAgent;

            string userAgentAttributeString = userAgent.Substring(userAgent.IndexOf("(")).Trim(new[] { '(', ')' });
            string[] userAgentAttributes = userAgentAttributeString.Split(new[] { ';' }, 4);
            switch (part)
            {
                case "device":
                    return userAgentAttributes[1];
                case "appversion":
                    return userAgent.Substring(0, (userAgent.IndexOf("(")));
                case "os":
                    return userAgentAttributes[0];
                default:
                    return userAgent;
            }
        }

        #endregion
    }
}
