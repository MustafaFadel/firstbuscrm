using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

using Amazon.Lambda.Core;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using Amazon;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace AWSLambda1
{
    public class Function
    {
        
        /// <summary>
        /// A simple function that takes a string and does a ToUpper
        /// </summary>
        /// <param name="input"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public object FunctionHandler(object input, ILambdaContext context)
        {
            JObject inp = JObject.Parse(input.ToString());

            string mongoString = Environment.GetEnvironmentVariable("mongoconnectionuri"); //""
            string envAm = Environment.GetEnvironmentVariable("am");
            string envPm = Environment.GetEnvironmentVariable("pm");
            string envDaysInPast = Environment.GetEnvironmentVariable("daysinpast");
            string envEnd = Environment.GetEnvironmentVariable("end") ?? string.Empty;
            string envEmails = Environment.GetEnvironmentVariable("recipients");
            string envDebug = Environment.GetEnvironmentVariable("debug");

            bool am = envAm == "1";
            bool pm = envPm == "1";
            int daysInPast = int.Parse(envDaysInPast);
            DateTime end;
            bool hasEnd = DateTime.TryParse(envEnd, out end);
            end = hasEnd ? end : DateTime.MinValue;
            string[] recipients = envEmails.Split(new char[] { ',' });
            bool debug = !string.IsNullOrEmpty(envDebug) && envDebug == "1" ? true : false;

            FirstBusCRMComponent crm = new FirstBusCRMComponent(am, pm, end, daysInPast, debug, mongoString, recipients);
            return crm.Log;

        }
    }
}
